Upload Arbitrary Waveforms to GH-CJDS66 60MHz  
---------------------------------------------  
  
Copyright 2020 Jeremy Carter <jeremy@jeremycarter.ca>  
  
Usage: ./dds-wave-upload.sh /dev/ttyUSB0 flat.txt 1  
  
The first argument is the target device to upload to.  
  
The second argument is the waveform file. It should be 2048 lines long, each line containing a number from 0 - 4096, specifying the amplitude of the waveform.  
  
The third argument is the preset you want to save the waveform in on the device. It must be a number from 1 - 15.  
  
Purpose:  
-------  
  Upload a user-defined arbitrary waveform
  to the GH-CJDS66 60MHz Koolertron DDS Signal 
  Generator/Counter.  
  
USB Interface:  
-------------  
  ID 1a86:7523 QinHeng Electronics CH340 serial converter  
  
How To:  
------  
  Design a waveform in the range of
  0 - 4096, made from 2048 points.  
  
  Save it in a text file with each
  point on its own line.  
  
  Run this script to upload it to the signal 
  generator, passing the waveform file as
  the second argument, and the preset to
  save it in as the third argument.  
  
  
Tutorial - Design a Wave:  
------------------------  
1. Download Waveform Manager Plus (this has been tested with v4.13):  
     https://www.aimtti.com/resources/waveform-manager-plus-v413  
  
2. Unzip it and install it with wine (it's Windows-only but works well in wine):  
     wine start waveman.msi  
  
3. Run the program with wine:  
     cd ~/".wine/drive_c/Program Files (x86)/Waveman"  
     wine waveman.exe  
  
4. Design a new waveform of amplitude 4096 and length 2048, and save it 
   as format "WaveCAD *.wav".  
  
5. Use the wav2txt.sh script in this project to convert the wav file to the 
   proper txt format:  
     ./wav2txt.sh testwave1.wav > testwave1.txt  
  
6. Now you can use the dds-wave-upload.sh script to upload your waveform
   to the device:  
     ./dds-wave-upload.sh /dev/ttyUSB0 testwave1.txt 1  
  
