#!/bin/sh
#
# Convert a wav or WaveCAD file to text
#
# Copyright 2020 Jeremy Carter <jeremy@jeremycarter.ca>
#
# LICENSE:
#   This script might break your computer or other 
#   devices, if it does, I'm very sorry, but the 
#   copyright holders will take no legal responsibility 
#   for any use of this script or related files. You 
#   are free to do whatever you want with it, but if 
#   you modify it, please add your name to the copyright 
#   area above, and you must leave Jeremy Carter listed 
#   as the first copyright holder, and this license
#   message must remain unaltered near the top of this 
#   file.
# END LICENSE
#
# Purpose:
# -------
#   Convert a wav or WaveCAD file to the proper format
#   for uploading to the GH-CJDS66 60MHz device.
#
#   You can make these wav files in a program called
#   Waveform Manager Plus:
#
#   https://www.aimtti.com/resources/waveform-manager-plus-v413
#
#   See the README.md file for more info.
#

INFILE="$1"

usage() {
	printf "\nConvert wav or WaveCAD file to text\n"
	printf "\nCopyright 2020 Jeremy Carter <jeremy@jeremycarter.ca>\n"

	printf "\nUsage: $0 testwave1.wav > testwave1.txt\n\n"
}

if [ $# -lt 1 -o ! -f "$INFILE" ]; then
	printf "\nError: file not found\n"
	usage
	exit 1
fi

for i in `od -An -vtd2 -w2 $INFILE`; do 
	o=`expr $i + 2048`
	printf "%d\n" "$o"
done

