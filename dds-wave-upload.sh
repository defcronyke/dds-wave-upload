#!/bin/bash
#
# Upload Arbitrary Waveforms to GH-CJDS66 60MHz
#
# Copyright 2020 Jeremy Carter <jeremy@jeremycarter.ca>
#
# LICENSE:
#   This script might break your computer or other 
#   devices, if it does, I'm very sorry, but the 
#   copyright holders will take no legal responsibility 
#   for any use of this script or related files. You 
#   are free to do whatever you want with it, but if 
#   you modify it, please add your name to the copyright 
#   area above, and you must leave Jeremy Carter listed 
#   as the first copyright holder, and this license
#   message must remain unaltered near the top of this 
#   file.
# END LICENSE
#
# Purpose:
# -------
#   Upload a user-defined arbitrary waveform
#   to the GH-CJDS66 60MHz Koolertron DDS Signal 
#   Generator/Counter.
#
# USB Interface:
# -------------
#   ID 1a86:7523 QinHeng Electronics CH340 serial converter
#
# How To:
# ------
#   Design a waveform in the range of
#   0 - 4096, made from 2048 points.
#
#   Save it in a text file with each
#   point on its own line.
#
#   Run this script to upload it to the signal 
#   generator, passing the waveform file as
#   the second argument, and the preset to
#   save it in as the third argument.
#

usage() {
	printf "\nUpload Arbitrary Waveforms to GH-CJDS66 60MHz\n"
	printf "\nCopyright 2020 Jeremy Carter <jeremy@jeremycarter.ca>\n"

	printf "\nUsage: $0 /dev/ttyUSB0 flat.txt 1\n"

	printf "\nThe first argument is the target device to upload to.\n"
	
	printf "\nThe second argument is the waveform file. It should be 2048 lines long, "
	printf "each line containing a number from 0 - 4096, specifying the amplitude of the waveform.\n"
	
	printf "\nThe third argument is the preset you want to save the waveform in on the device. "
	printf "It must be a number from 1 - 15.\n\n"
}

# If wrong number of args.
if [ $# -lt 3 ]; then
	usage
	exit 1
fi

DEVICE="$1"
BAUD_RATE=115200
MODES="raw"

# Set up serial interface.
stty -F "$DEVICE" $BAUD_RATE $MODES

# The waveform file.
IN_FILE="$2"

# Points should be separated by newline 
# character, one point per line.
IN_DELIMITER="\n"

# The device's communication protocol expects 
# the data points to be separated by commas, 
# with a period after the last data point.
OUT_DELIMITER=","
OUT_STOP="."

# The device expects to receive this value to 
# tell it when we are finished uploading the 
# waveform.
OUT_TERMINATE="\x0a"

# Save in device's preset.
PRESET=$3

# If preset argument is out of range.
if [ $PRESET -lt 1 -o $PRESET -gt 15 ]; then
	usage
	exit 2
fi

# Add a 0 at the beginning of the preset 
# number if it's less than 10.
if [ $PRESET -lt 10 ]; then
	PRESET="0$PRESET"
fi

# The command to send to the device. The 
# following is for arbitrary waveform 
# upload/write to preset #1:
#   :a01=
COMMAND=":a$PRESET="

# Get the waveform data from its text file, and save it into
# a slightly different format, which is close to what the 
# device expects to receive.
DATA=`cat "$IN_FILE" | tr "$IN_DELIMITER" "$OUT_DELIMITER" | rev | sed "s/$OUT_DELIMITER/$OUT_STOP/" | rev`

# Number of elements in a chunk to upload together.
STRIDE=128

# A var to track the chunk's starting position.
STARTFIELD=1

# A var to track the chunk's ending position.
ENDFIELD=`expr $STRIDE + 1`

# Number of waveform data chunks to send.
CHUNKS=16

# Delay between sending each chunk in seconds.
DELAY=0.01

# A loop counter to track which iteration we are 
# on while sending chunks.
i=0

# Send the waveform data to the device in chunks.
while [ $i -lt $CHUNKS ]; do
	# Get 128 of the data points to upload in a chunk together.
	CHUNK="`printf "$DATA" | cut -d, -f$STARTFIELD-$ENDFIELD`,"

	if [ $i -eq 0 ]; then
		# First data chunk starts with the command.
		CHUNK="$COMMAND$CHUNK"
	fi

	if [ $i -eq `expr $CHUNKS - 1` ]; then
		# Remove an extra comma on the end of the data.
		CHUNK=`printf "$CHUNK" | rev | sed 's/,//' | rev`

		# Terminate the upload communication with \x0a.
		CHUNK="$CHUNK$OUT_TERMINATE"
	fi

	# Write the data chunk to the device.
	printf "$CHUNK" > "$DEVICE"
	
	# Move to next chunk start position for next iteration.
	STARTFIELD=$ENDFIELD

	# Move to next chunk end position for next iteration.
	ENDFIELD=`expr $ENDFIELD + $STRIDE + 1`

	# Increment loop counter.
	i=`expr $i + 1`
	
	# Delay between sending each data chunk.
	sleep $DELAY
done

